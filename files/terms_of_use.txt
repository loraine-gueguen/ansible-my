﻿Charte d'utilisation des infrastructures fédérées par l'Institut Français de Bioinformatique (IFB)

29/08/2019

La présente charte a pour objet de définir les conditions d’utilisation et les règles de bon usage des serveurs, clusters de calcul, espaces disques et services webs des sites fédérés par l'IFB via le NNCR (https://www.france-bioinformatique.fr/fr/infrastructure-0). La présente charte est subordonnée à la charte informatique propre au site de l'IFB accédé et à celle de son établissement hébergeur. Elle est susceptible d'évoluer en fonction des règles et pratiques de la fédération des sites impliqués et de leurs établissements hébergeurs.

L'IFB fournit un accès à :
* Des ressources de calcul et de stockage,
* Des services incluant la mise à disposition de banques de données et de logiciels « open source », des cycles d'apprentissages / formation, l'hébergement de sites web et de machines virtuelles, une expertise et un support scientifique à des projets relevant de la biologie ou de la bioinformatique.
L'infrastructure mise à disposition est destinée aux biologistes, bioinformaticiens et bio-statisticiens ayant des besoins de calcul intensif de volumétrie moyenne (qq 100aine de Go) et d'un environnement bioinformatique (logiciels, banques de données, support ...). L'IFB se réserve le droit de renvoyer certains projets sur les très grands centres de calcul nationaux lorsque la situation le justifiera.

Conditions d'accès et limites d'utilisation de l'infrastructure

Accès à l'infrastructure

Pour bénéficier des ressources informatiques de l'IFB, une demande d’accès doit être effectuée par un formulaire de création de compte accessible via le portail national ou l'un des sites fédérés par l'IFB. Cette demande est soumise à une validation par l'équipe du site et à l'acceptation de la présente charte, de celles du site accédé et de son établissement hébergeur. L’autorisation d’accès accordée est strictement personnelle, incessible et temporaire. Elle sera retirée dès lors que la fonction de l’utilisateur ne le justifie plus ou en cas de non respect de la présente charte ou de celles du site accédé ou de son établissement hébergeur.

Engagements de l'IFB

Chaque compte utilisateur sur un site fédéré par l'IFB donne droit aux différents services.
1. Un accès au serveur de login : le mot de passe du compte doit être renouvelé tous les 12 mois. Tout compte inutilisé pendant une période de 12 mois sera désactivé. Tout utilisateur sera alerté de la clôture de son compte à l'adresse e-mail associée à son compte. Les données de son espace seront effacées en regard des règles stipulées par la charte du site accédé.
2. L'affectation d'espaces de stockage et quota : l'affectation de l'espace de stockage est subordonnée aux possibilités/capacités des sites accédés. Selon les sites accédés, plusieurs types d’espace sont disponibles : des espaces projets répliqués ou non, sauvegardés ou non et des espaces temporaires (dit scratch) où les données non accédées depuis plus d’un délai fixé par le site accédé sont purgées. Pour disposer de plus d'espace disque, selon le site, il est nécessaire de requérir à une demande exceptionnelle en s'adressant aux administrateurs systèmes du site accédé ou de l’équipe d’administration du NNCR. Celle-ci sera examinée par la direction du site accédé, et pourra, selon les cas, impliquer l'établissement d'un devis.
3. L’accès aux banques de données du domaine.
4. Des environnements logiciel bioinformatique.
5. Un support : dans les limites de la disponibilité des agents en charge du support ;
6. Un accès au calcul (cluster ou cloud) : l'IFB s'engage à faire de son mieux pour limiter au maximum les interruptions de service, mais ne peut pas garantir la disponibilité permanente des ressources. L'IFB s'engage à prévenir les utilisateurs des maintenances matérielles et systèmes planifiées impliquant une coupure de service.
7. Quota de calcul : le quota de calcul autorisé est variable selon les sites. Pour disposer de plus de temps de calcul, il est nécessaire de requérir à une demande exceptionnelle en s'adressant aux administrateurs systèmes du site accédé ou de l’équipe d’administration du NNCR. Celle-ci sera examinée et pourra impliquer l'établissement d'un devis.

Installation de logiciels ou banques de données génomiques

L’installation et l’utilisation de logiciels ou de banques par les utilisateurs doit se faire dans le respect des dispositions du code de la propriété intellectuelle. En particulier, l’utilisateur doit être en mesure de présenter une licence en règle en cas de demande (cela vaut également pour les utilisateurs de Galaxy). L’installation de logiciels ou utilitaires pouvant porter atteinte à l’intégrité des systèmes n’est pas autorisée.
Une demande d'installation multi-users de logiciels ou de banques peut être faite au support du site accédé. L'installation sera réalisée par l'équipe d'administration système de la plate-forme dans la limite des possibilités : pertinence, temps, ressources nécessaires, dépendances logicielles.

Conditions d'utilisation de l'infrastructure

Soumission des programmes de calcul

Les ressources de calcul disposent d'un gestionnaire de tâches ou ordonnanceur qui peut être différent selon les sites. Les programmes des utilisateurs doivent être soumis en utilisant le gestionnaire de tâches disponible sur le site accédé. Le contournement de cet ordonnanceur et de ses règles de priorité peut donner lieu à une suspension immédiate du programme soumis et à une éventuelle suppression de l'accès à l'infrastructure.
L'utilisateur est responsable des tâches qu'il lance tant sur le serveur de login que sur les ressources de calcul . Il doit les suivre et les supprimer en cas de problème.
Les serveurs de login sont réservés exclusivement à la connexion, au transfert de données, à la compilation, au test de la ligne de commande et à la soumission de jobs sur le cluster de calcul. Tout traitement de données lancé directement sur les serveurs de login sera systématiquement interrompu sans préavis par les administrateurs système.

Stockage des données

Chaque utilisateur est tenu de n'utiliser que la quantité d'espace disque qui lui est strictement nécessaire, et d'utiliser efficacement les moyens de compression et d'archivage. Les fichiers temporaires de calcul doivent être supprimés à l’issue du calcul.
IL EST DE LA RESPONSABILITÉ DE L'UTILISATEUR DE GÉRER SES DONNÉES (organisation, volumétrie, pertinence, ancienneté). Le NNCR n’assure pas de service d’archivage des données

Informations et Mot de passe

* Chaque utilisateur s'engage à communiquer au support tout changement ou complément d'informations le concernant (mail, laboratoire, numéro de téléphone, groupe...) .
* Chaque utilisateur s'engage à protéger son compte informatique sur le cluster de calcul par un mot de passe, qu'il renouvellera régulièrement (idéalement annuellement).
* L'utilisateur est responsable des dommages qui pourraient provenir d'une mauvaise utilisation de son compte s'il est établi qu'il en a donné le mot de passe.

Informations aux utilisateurs

L'information aux utilisateurs sur l’administration des ressources (changements de politique, mise en place de nouveaux services, interruptions de service...) se fait au moyen des outils de communication du site accédé (newsletter, mails d'avertissements,…). L'adresse mail utilisée pour une communication par email sera celle communiquée par l’utilisateur lors de sa demande de création de compte. Les utilisateurs s'engagent à se conformer aux différentes directives communiquées par l’équipe d’administration du NNCR et/ou les administrateurs des plateformes de l’IFB grâce aux moyens sus-cités.

Respect du caractère confidentiel des informations

Lors de leur création, les répertoires des utilisateurs ont des droits par défaut conformément aux pratiques du site accédé. Il appartient à l'utilisateur de gérer ses droits comme il le souhaite en changeant les droits par défaut (commande ‘chmod’) . Les fichiers de chacun sont privés.
Toute tentative de lecture ou de copie de fichiers d’un autre utilisateur sans son autorisation, de même que l’interception de communications entre utilisateurs est répréhensible et pourra, entre autre, entraîner la suppression de l’accès à l’infrastructure.

Propriété et valorisation des résultats

Les résultats issus de l'utilisation des ressources de l'IFB sont la propriété exclusive du laboratoire de recherche auquel l’utilisateur est affilié. Le laboratoire de recherche a la maîtrise de la valorisation des résultats, toutefois, les équipes de recherche s'engagent à remercier l'IFB et le site accédé dans leur(s) publication(s) et communication(s), en utilisant la phrase suivante : We are grateful to the Institut Français de Bioinformatique and the « name_of_the_ platform » platform for providing help and/or computing and/or storage resources". Dans le cadre d’une collaboration ne faisant pas l’objet de prestations, l’équipe s’engage à valoriser les développements et les analyses en associant à d’éventuelles publications les ingénieurs du NNCR ou de la plateforme.

Déontologie

* Toute utilisation des ressources implique l'acceptation de la présente charte, de la charte du site accédé et de son organisme hébergeur.
* Les responsables des ressources se réservent le droit d’empêcher l’accès à un utilisateur en cas de dépassement de quotas, ou en cas de non respect des règles énoncées dans la présente charte, ou dans la charte du site accédé ou celle de son organisme hébergeur.
* Dans le cas de travaux confidentiels, il est de la responsabilité de l'utilisateur de garantir leur confidentialité.
* Le personnel du site accédé ne peut en aucune façon être considéré comme responsable soit de la perte de ces données, soit de leur diffusion.

Règlement général sur la protection des données RGPD/GPRD

Avec l'entrée en vigueur du Règlement Général sur la Protection des Données (RGPD) depuis le 25 mai 2018, nous souhaitons vous informer en toute transparence sur la collecte et le traitement de vos données personnelles réalisé par les sites fédérés par l'IFB via le NNCR.
L’ouverture d’un compte, pour accéder au cluster, pour utiliser l’une des instances web ou l’une des bases de données hébergées par la plate-forme, nous amène à collecter les informations personnelles suivantes : nom, prénom, affiliation, adresse mél, et éventuellement numéro de téléphone.
Ces données, ainsi que les celles que vous êtes susceptible de déposer dans l’espace de stockage qui vous est attribué, ne sont en aucune manière rendues publiques ou communiquées à des tiers.
Les logs de connexion sur nos serveurs sont conservés pour des raison légales durant un an.
Vous êtes en droit de nous adresser à tout moment une demande de communication, modification ou suppression de vos données personnelles et scientifique, en utilisant l’adresse mél de contact de la plateforme utilisée.

________________

Charter for the use of infrastructures federated by the French Institute of Bioinformatics (IFB)

29/08/2019

The purpose of this charter is to define the conditions of use and rules for the proper use of servers, computing clusters, disk space and web services of sites federated by the IFB via the NNCR (https://www.france-bioinformatique.fr/en/infrastructure-0). This charter is subject to the IT charter specific to the IFB site accessed and of its host institution. It is likely to evolve according to the rules and practices of the federation of the sites involved and their host establishments.

The IFB provides access to:
* Computing and storage resources,
* Services including the provision of open source databases and software, learning / training cycles, hosting of websites and virtual machines, expertise and scientific support for projects in the field of biology or bioinformatics.
The infrastructure provided is intended for biologists, bioinformaticians and bio-statisticians with intensive computation needs of average volumes (hundreds of GB) and a bioinformatics environment (software, databases, support...). The IFB reserves the right to refer some projects to very large national computing centers when the situation warrants.

Access conditions and infrastructure usage limits

Access to infrastructure

To benefit from the IT resources of the IFB, an access request must be made via an account creation form accessible on the national portal or on one of the sites federated by the IFB. This request is subject to validation by the site team and acceptance of this charter, those of the accessed site and its host institution. The access authorization granted is strictly personal, non-transferable and temporary. It will be withdrawn as soon as the user's function no longer justifies it or in the event of non-compliance with this charter or those of the accessed site or its host establishment.

Commitments of the IFB

Each user account on a site federated by the IFB gives access to various services.
1. Access to the login server: the account password must be renewed every 12 months. Any account that is unused for a period of 12 months will be deactivated. Any user will be notified when their account is closed at the e-mail linked to their account. The data in its space will be deleted in accordance with the rules stipulated by the charter of the accessed site.
2. Storage space and quota allocation: the allocation of storage space is subject to the possibilities/capacities of the accessed sites. Depending on the accessed sites, several types of working space are available: replicated or not, backed up or not project spaces and temporary spaces (called scratch) where data not accessed for more than a period of time set by the accessed site is purged. To have more disk space, depending on the site, it is necessary to make a request to the system administrators of the accessed site or the NNCR administration team. This will be examined by the management of the accessed site, and may, depending on the case, involve the preparation of a quotation.
3. Access to the domain's databanks.
4. Bioinformatics software environments.
5. Support: within the limits of the availability of the agents in charge of support
6. Access to computing (cluster or cloud): the IFB undertakes to do its best to limit service interruptions as much as possible, but cannot guarantee the permanent availability of resources. The IFB undertakes to notify users of planned hardware and system maintenance involving a service interruption.
7. Calculation quota: the authorized calculation quota varies according to the sites. To have more computing time, it is necessary to make a request to the system administrators of the accessed site or the NNCR administration team. This will be examined and may involve the preparation of a quotation.

Installation of software or genomic databanks

The installation and use of software or banks by users must comply with the provisions of the intellectual property code. In particular, the user must be able to present a valid license upon request (this also applies to Galaxy users). The installation of software or utilities that may affect the integrity of the systems is not allowed.
A request for multi-user installation of software or banks can be made to the support team of the accessed site. The installation will be carried out by the platform's system administration team depending on several factors: relevance, time, necessary resources, software dependencies.

Conditions of use of the infrastructure

Submission of calculation programs

The calculation resources have a task manager or scheduler that can differ from site to site. User programs must be submitted using the task manager available on the accessed site. Bypassing this scheduler and its priority rules may result in an immediate suspension of the submitted program and possible removal of access to the infrastructure.
Users are responsible for the tasks they launch both on the login server and on the calculation resources. They must follow them and delete them in case of problems.
Login servers are reserved exclusively for connection, data transfer, compilation, command line testing and job submission on the computing cluster. Any data processing launched directly on the login servers will be systematically interrupted without notice by the system administrators.

Data storage

Each user is required to use only as much disk space as is strictly necessary, and to use compression and archiving facilities efficiently. Temporary calculation files must be deleted at the end of the calculation.
IT IS THE USER'S RESPONSIBILITY TO MANAGE ITS DATA (organization, volume, relevance, life cycle). NNCR does not provide a data archiving service.

User information and Password

* Each user undertakes to communicate to the support any change or additional information concerning them (email, laboratory, telephone number, group...).
* Each user undertakes to protect their computer account on the computing cluster with a password, which they will renew regularly (ideally annually).
* The user is responsible for any damage that may result from misuse of their account if it is established that they have given the password.

Information to users

Information to users on resource administration (policy changes, implementation of new services, service interruptions, etc.) is provided using the communication tools of the accessed site (newsletter, warning emails, etc.). The email address used for an email communication will be the one provided by the user when they request to create an account. Users undertake to comply with the various guidelines communicated by the NNCR administration team and/or the IFB platform administrators using the above-mentioned means.

Respect data confidentiality

When created, user directories have default rights in accordance with the practices of the accessed site. It is up to users to manage their rights as they wish by changing the default rights ('chmod' command). Everyone's files are private.
Any attempt to read or copy another user's files without their permission is reprehensible, as is the interception of communications between users.

Ownership and valuation of results

The results from the use of IFB resources are the exclusive property of the research laboratory. The research laboratory is in control of the valorisation of the results, however, the research teams undertake to thank the IFB and the site accessed in their publication(s) and communication(s), using the following sentence: We are grateful to the French Institute of Bioinformatics and the "name_of_the_platform" platform for providing help and/or computing and/or storage resources". In the context of a collaboration that is not the subject of services, the team undertakes to enhance the value of developments and analyses by involving the engineers of the NNCR or the platform in any publications.

Deontology

* Any use of the resources implies the acceptance of this charter, the charter of the accessed site and its host organization.
* The resource managers reserve the right to prevent access to a user in the event of exceeding quotas, or in the event of non-compliance with the rules set out in this charter, or in the charter of the accessed site or that of its host organization.
* In the case of confidential work, it is the user's responsibility to guarantee its confidentiality.
* The staff of the accessed site can in no way be considered responsible for either the loss of this data or its distribution.

General Data Protection Regulations GPRD/RGPD

With the entry into force of the General Data Protection Regulations (GPRD) on 25 May 2018, we would like to inform you in all transparency about the collection and processing of your personal data by the sites federated by the IFB via the NNCR.
Opening an account, to access the cluster, to use one of the web instances or one of the databanks hosted by the platform, leads us to collect the following personal information: last name, first name, affiliation, e-mail address, and possibly telephone number.
These data, as well as those that you may deposit in the storage space allocated to you, are in no way made public or communicated to third parties.
Connection logs on our servers are kept for legal reasons for one year.
You are entitled to send us at any time a request for communication, modification or deletion of your personal and scientific data, using the contact email address of the platform used.
