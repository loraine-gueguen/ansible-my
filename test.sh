#!/bin/bash
#set -x
set -e

ansible-galaxy install --force -r molecule/default/requirements.yml
rm -f molecule/default/roles
ln -s $(pwd)/.. molecule/default/roles


ansible-playbook -v -i 192.168.105.165,192.168.105.166, -u root  molecule/default/ugly.delete.yml

# warning the , is mandatory... 192.168.105.166,
ansible-playbook -v -i 192.168.105.165,192.168.105.166, -u root --extra-vars '{
                 openldap_server_force_reload_root_dn: true,
                 openldap_server_domain_name: "ifb.local",
                 openldap_server_root_name: "admin",
                 openldap_server_root_pw: "my"
                 }' molecule/default/prepare.yml

#with-nunjucks,
ansible-playbook -v -i 192.168.105.165,192.168.105.166, -u root --extra-vars '{
                 my_git_force: yes,
                 my_git_update: yes,
                 my_git_version: wip-ifb-tmpl,
                 my_group_in_path: "true",
                 my_openldap_dn: "dc=ifb,dc=local",
                 my_smtp_host: "192.168.103.247",
                 my_admin_email: "francois.gerbes@france-bioinformatique.fr"
                 }' molecule/default/playbook.yml

ansible-playbook -v -i 192.168.105.165,192.168.105.166, -u root --extra-vars '{
                 ldap_cn: "cn=admin,dc=ifb,dc=local"
                 }' molecule/default/ugly.getinfo.yml


# ldapadd -c -x -h localhost -D 'cn=admin,dc=ifb,dc=local' -w my -f test_import/export_phpldapadmin.ldif
# npm install command-line-args command-line-usage
# node -r dotenv/config import_from_ldap.js --import -a admin
